<?php

/**
 * Подключение к БД, запросы
 *
 * @autor Рассохин Алексей <telegram: @alras63>
 *
 * */
final class Database {
    public static $db_config = array(
        'connection' => 'mysql',
        'host' => 'localhost',
        'database' => 'akticom_rassohin',
    
        'username' => 'root',
        'password' => 'root'
    );
    public static $pdo = null;

    public static function connect() {
        $connectString = Database::$db_config['connection'].':host='.Database::$db_config['host'].';dbname='.Database::$db_config['database'];
        
        try {
            Database::$pdo = new PDO($connectString, Database::$db_config['username'], Database::$db_config['password']);
            return true;
        } 
        catch (PDOException $e) {
            return false;
        }
    }

    public static function execute(string $query, array $vars = []) {
        $stmt = Database::$pdo->prepare($query);
        return $stmt->execute($vars);
    }

    public static function executeFetch(string $query, array $vars = []) {
        $stmt = Database::$pdo->prepare($query);
        $stmt->execute($vars);
        return $stmt->fetch(PDO::FETCH_ASSOC);
    }

    public static function executeFetchAll(string $query, array $vars = []) {
        $stmt = Database::$pdo->prepare($query);
        $stmt->execute($vars);
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }
}