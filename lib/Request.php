<?php

/**
 * HTTP библиотека
 *
 * @autor Рассохин Алексей <telegram: @alras63>
 *
 * */
final class Request {
    public static function cors() {

        header("Access-Control-Allow-Origin: *");
        header("Content-Type: application/json; charset=UTF-8");
        header("Access-Control-Allow-Methods: POST, GET, PUT, DELETE, PATCH");
        header("Access-Control-Allow-Headers: *");
    }

    public static function decodeUrl($url, $files = null): array
    {
        $result = array();

        foreach (explode('&', $url) as $chunk) {
            $param = explode("=", $chunk);
        
            if ($param) {
                $result[urldecode($param[0])] = urldecode($param[1]);
            }
        }

        if(null !== $files) {
	        foreach ($files as $key=>$file) {
		        $result[$key] = $file;
	        }
        }

        return $result;
    }

    public static function getFields() {
        $method = $_SERVER['REQUEST_METHOD'];
        
        if($method == 'GET')
            return $_GET;
        else {
            $data  = file_get_contents('php://input');
            $files = $_FILES;

            if(Utils::str_starts_with($data, '{')) {
                return json_decode($data, true);
            }

            else {
                return Request::decodeUrl($data, $files);
            }
        }
    }

    public static function requireFields(array $fields): bool
    {
        $empty = [];
        $fieldsInRequest = Request::getFields();
        
        if(!isset($fieldsInRequest)) {
            Request::finishErrorServer('Пустые поля');
            return false;
        }

        foreach($fields as &$fieldName) {
            if(!isset($fieldsInRequest[$fieldName])) {
                $empty[$fieldName] = 'Поле ' . $fieldName . ' не должно быть пустым';
            }
        }

        if(count($empty) > 0) {
            Request::finishErrorValidation($empty);
            return false;
        }
        else { return true; }
    }
    
    public static function finishSuccess(array $result, int $code = 200) {
        Request::result($result, $code);
    }

    public static function finishError(string $message, int $code) {
        Request::result(array(
            'error' => array(
                'code' => $code,
                'message' => $message
            )
        ), $code);
    }
    public static function finishErrorUnauthorized() {
        Request::finishError('Login failed', 403);
    }
    public static function finishErrorForbidden() {
        Request::finishError('Forbidden for you', 403);
    }
    public static function finishErrorValidation(array $fieldsErrors) {
        Request::result(array(
            'error' => array(
                'code' => 422,
                'message' => 'Validation error',
                'errors' => $fieldsErrors
            )
        ), 422);
    }
    public static function finishErrorServer($message) {
        Request::finishError('Ошибка сервера: ' . $message, 500);
    }

    private static function result(array $json, int $code = 200) {
        http_response_code($code);
        echo json_encode($json);

        exit;
    }

	public static function renderHtml($template) {
		echo file_get_contents($template);
	}
} 