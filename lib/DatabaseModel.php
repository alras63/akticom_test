<?php

/**
 * Модель для работы с базой данных
 *
 * @autor Рассохин Алексей <telegram: @alras63>
 *
 * */
abstract class DatabaseModel {
    abstract public function getTableName();
    abstract public function getFields();

	public static function describe($tabelName) {
    	$sql =  "DESCRIBE `$tabelName`";
		return Database::execute($sql);
	}

	/**
	 * Создает таблицу на основе моделей
	 *
	 * @autor Рассохин Алексей <telegram: @alras63>
	 *
	 * */
    public static function migrate(DatabaseModel $model) {
        $sql = 'CREATE TABLE ' . $model->getTableName() . ' (';

        $fields = $model->getFields();
        $i = 0;
        $length = count($fields);
        foreach($fields as &$field) {

            $sql .= $field['name'] . ' ' . $field['type'];

            if(isset($field['length'])) { $sql .= ' (' . strval($field['length']) . ') '; }

            if($field['type'] == 'INT') {  $sql .= ' UNSIGNED '; }

            if(array_key_exists('auto_increment', $field) && $field['auto_increment'] == true) {
                $sql .= 'AUTO_INCREMENT ';
            }
            if(array_key_exists('nullable', $field) && $field['nullable'] == true) {
                $sql .= ' NULL ';
            }

            if(array_key_exists('primary', $field) && $field['primary'] == true) {
                $sql .= 'PRIMARY KEY ';
            }

            if($length > 1 && $i < $length - 1) { $sql .= ','; }

            $i++;
        }
        $sql .= ');';

        return Database::execute($sql);
    }

	/**
	 * SQL SELECT
	 *
	 * @autor Рассохин Алексей <telegram: @alras63>
	 *
	 * */

	public static function select(DatabaseModel $model, array $where, bool $multiple = false) {
		$sql = 'SELECT * FROM ' . $model->getTableName() . ' WHERE ';

		$i = 0;
		$length = count($where);
		foreach(array_keys($where) as &$colName) {
			$sql .= $colName . "=? ";

			if($length > 1 && $i < $length - 1) { $sql .= ','; }
			$i++;
		}

		if($multiple)
			return Database::executeFetchAll($sql, array_values($where));
		else
			return Database::executeFetch($sql, array_values($where));
	}

	/**
	 * SQL SELECT
	 *
	 * @autor Рассохин Алексей <telegram: @alras63>
	 *
	 * */
	public static function selectAll(DatabaseModel $model): array {
		$sql = 'SELECT * FROM ' . $model->getTableName();
		return Database::executeFetchAll($sql);
	}

	/**
	 * SQL INSERT
	 *
	 * @autor Рассохин Алексей <telegram: @alras63>
	 *
	 * */
    public static function insert(DatabaseModel $model, array $values) {
        $sql = 'INSERT INTO ' . $model->getTableName() . ' (';
        $length = count($values);

        $keys = array_keys($values);

	    $i = 0;
        foreach($keys as &$colName) {
            $sql .= $colName;
            if($length > 1 && $i < $length - 1) { $sql .= ','; }
            $i++;
        }
        $sql .= ') VALUES (';

        $i = 0;
        foreach(array_values($values) as &$colValue) {
            if(is_string($colValue) && Utils::str_starts_with($colValue, 'JSON')) {
                $sql .=  $colValue;

                unset($values[$keys[$i]]);
            }
            else {
                $sql .= '?';
            }

            if($length > 1 && $i < $length - 1) { $sql .= ','; }
            $i++;
        }
        $sql .= ');';

        try {
            return Database::execute($sql, array_values($values));
        }
        catch(PDOException $e) {
	        print_r($e->errorInfo[1]);
        }

        return false;
    }
}