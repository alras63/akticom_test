<?php

/**
 * Класс полезных утилит, которые понадобятся в работе
 *
 * @autor Рассохин Алексей <telegram: @alras63>
 *
 * */
final class Utils {
    /**
     * Проверяет, начинается ли файл с опеределенных символов
     *
     * @autor Рассохин Алексей <telegram: @alras63>
     *
     * */
    public static function str_starts_with(string $haystack, string $needle): string {
        return (string)$needle !== '' && strncmp($haystack, $needle, strlen($needle)) === 0;
    }

	/**
	 * Утилита для загрузки файлов на сервер
	 *
	 * @autor Рассохин Алексей <telegram: @alras63>
	 *
	 * */
    public static function upload_file($file) {
	    $uploaddir = 'uploads/';
	    $uploadfile = $uploaddir . basename($file['name']);

	    if (move_uploaded_file($file['tmp_name'], $uploadfile)) {
		    return $uploadfile;
	    } else {
		    return false;
	    }
    }
}