<?php

/**
 * Модель позиций (товаров)
 *
 * @autor Рассохин Алексей <telegram: @alras63>
 *
 * */
class PositionsModel extends DatabaseModel {
    public static function getInstance() {
        return new PositionsModel();
    }

    public function getTableName() {
        return 'positions';
    }
    public function getFields() {
        return [
	        array(
		        'name' => 'id',
		        'type' => 'INT',
		        'primary' => true,
		        'auto_increment' => true
	        ),
            array(
                'name' => 'code_item',
                'type' => 'VARCHAR',
                'length' => 50,
            ),
	        array(
                'name' => 'name_item',
                'type' => 'VARCHAR',
                'length' => 255,
            ),
            array(
                'name' => 'level_one',
                'type' => 'VARCHAR',
                'length' => 100,
            ),
	        array(
                'name' => 'level_two',
                'type' => 'VARCHAR',
                'length' => 100,
                'nullable' => true
            ),
	        array(
                'name' => 'level_three',
                'type' => 'VARCHAR',
                'length' => 100,
                'nullable' => true
            ),
            array(
                'name' => 'cost',
                'type' => 'FLOAT',
                'nullable' => true
            ),
            array(
                'name' => 'cost_sp',
                'type' => 'FLOAT',
                'nullable' => true
            ),
            array(
                'name' => 'count_item',
                'type' => 'INT',
                'nullable' => true
            ),
            array(
                'name' => 'polya',
                'type' => 'TEXT',
                'nullable' => true
            ),
            array(
                'name' => 'sp',
                'type' => 'INT',
                'nullable' => true
            ),
            array(
                'name' => 'unit',
                'type' => 'VARCHAR',
                'length' => 50,
                'nullable' => true
            ),
            array(
                'name' => 'img',
                'type' => 'TEXT',
                'nullable' => true
            ),
            array(
                'name' => 'show_on_main',
                'type' => 'INT',
                'nullable' => true
            ),
            array(
                'name' => 'description',
                'type' => 'TEXT',
                'nullable' => true
            )
        ];
    }
}