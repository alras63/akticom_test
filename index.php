<?php

// Library ----------------------------------
require __DIR__ . '/lib/Utils.php';
require __DIR__ . '/lib/Router.php';
require __DIR__ . '/lib/Request.php';
require __DIR__ . '/lib/Database.php';
require __DIR__ . '/lib/DatabaseModel.php';
// Models -----------------------------------
require __DIR__ . '/models/PositionsModel.php';

require __DIR__ . '/vendor/autoload.php';

// Проверяем подключение к БД
if (!Database::connect()) {
	Request::finishErrorServer('Ошибка подключения к БД');
	return;
}

// Роуты
$api = new Router();
$api->set404(function () {
	header('HTTP/1.1 404 Not Found');
	Request::finishError('Страница(endpoint) не найдена', 400);
});

$api->get('view', function () {
	header('Content-type: text/xml');

	$models = PositionsModel::selectAll(PositionsModel::getInstance());

	$fields = PositionsModel::getInstance()->getFields();

	$xml = new XMLWriter();

	$xml->openURI("php://output");
	$xml->startDocument();
	$xml->setIndent(true);

	$xml->startElement('positions');

	foreach ($models as $model) {
		$xml->startElement("position");

		foreach ($fields as $field) {
			$xml->writeElement($field['name'], $model[$field['name']]);
		}

		$xml->writeRaw($model['position']);

		$xml->endElement();
	}

	$xml->endElement();


	$xml->flush();

});

$api->get('upload', function () {
	Request::renderHtml('templates/form.html');

});


/**
 * Хочу предупредить. Никогда не делаю логику в роутах, но тут выносить ее в контроллер,
 * который вызывался бы роутом, было бы излишне, на мой взляд. Поэтому сделал так
 */
$api->post('api/insert_position', function () {
	Request::cors();

	if (Request::requireFields(['file'])) {
		$fields = Request::getFields();
		$file   = $fields['file'];

		$checkExist = DatabaseModel::describe('positions');

		if ($checkExist) {
			$uploaded = Utils::upload_file($file);


			if ($uploaded) {
				$columns = ['Код',
				            'Наименование',
				            'Уровень1',
				            'Уровень2',
				            'Уровень3',
				            'Цена',
				            'ЦенаСП',
				            'Количество',
				            'Поля свойств',
				            'Совместные покупки',
				            'Единица измерения',
				            'Картинка',
				            'Выводить на главной',
				            'Описание'
				];

				$columnsDict = ['code_item',
				            'name_item',
				            'level_one',
				            'level_two',
				            'level_three',
				            'cost',
				            'cost_sp',
				            'count_item',
				            'polya',
				            'sp',
				            'unit',
				            'img',
				            'show_on_main',
				            'description'
				];

				$reader = \League\Csv\Reader::createFromPath($uploaded, 'r+')
					->setHeaderOffset(0)
					->setEscape('"')
					->setDelimiter(',')
					->includeEmptyRecords();

				$cleanedCsv = str_replace('"', '', trim($reader->toString(), '\'"') );

				$reader = \League\Csv\Reader::createFromString($cleanedCsv)->setHeaderOffset(0)->setDelimiter(';')->skipEmptyRecords();

				$grab = [];

				try {
					foreach ($reader->getHeader() as $index => $column) {

						if (in_array($column, $columns)) {
							$grab[$index] = $column;
						}

					}
				} catch (\League\Csv\SyntaxError $exception) {
					 Request::finishError("Не удалось распарсить csv", 400);
				}

				if(0 === count($grab)) {
					Request::finishError("Не удалось распарсить csv", 400);
					return false;
				}

				foreach ($reader->getRecords($grab) as $i => $row) {

					if ($i == 0) {
						continue;
					}

					$row = array_filter($row, function($a) {
						return trim($a) !== "" && trim($a) !== NULL;
					});

					$filteredRow = [];
					foreach ($grab as $column => $index) {
						if(in_array($columnsDict[$column], ['cost', 'cost_sp'])) {
							$filteredRow[$columnsDict[$column]] = (float)$row[$index];
						} else if(in_array($columnsDict[$column], ['show_on_main', 'sp', 'count_item'])) {
							$filteredRow[$columnsDict[$column]] = (int) $row[$index];
						} else {
							$filteredRow[$columnsDict[$column]] = $row[$index];
						}
					}

					$model = PositionsModel::select(PositionsModel::getInstance(), ['code_item' => $filteredRow['code_item']]);

					if($model) {
						continue;
					}

					PositionsModel::insert(PositionsModel::getInstance(), $filteredRow);
				}

				Request::finishSuccess(['message' => "Успешно добавлено!"], 200);
			}
		} else {
			DatabaseModel::migrate(PositionsModel::getInstance());
		}
	}

});

$api->run();